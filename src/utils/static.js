export const CELL_STATUS={AVAILABLE:'available',UNAVAILABLE:'unavailable',OUT_OF_GRID:'outOfGrid'}
export const PLAYER_DISC = {WHITE:'white',BLACK:'black',NO_DISC:'noDisc'}
export const DIRECTION = {
    LEFT:{x:-1,y:0},
    RIGHT:{x:+1,y:0},
    UP: {x:0,y:+1},
    DOWN:{x:0,y:-1},
    DIAG_TOP_LEFT: {x:-1,y:+1},
    DIAG_TOP_RIGHT:{x:+1,y:+1},
    DIAG_BOTTOM_LEFT: {x:-1,y:-1},
    DIAG_BOTTOM_RIGHT: {x:+1,y:-1}
}
export const PLAYER = {
    PLAYER_ONE:"Player one",
    PLAYER_TWO:"Player two"
}