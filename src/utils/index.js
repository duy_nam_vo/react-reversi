import {PLAYER_DISC,CELL_STATUS,DIRECTION, PLAYER} from './static';

export function initialCellState(){
    return new Array(64).fill("").map((elem,idx)=>idx+1).map((elem,idx)=>{
        let x,y,playerdisc,cellStatus,xInternal = 0;
         if((elem)%8 === 0){
            xInternal = 72;
            x = String.fromCharCode(xInternal);
            y = parseInt(elem/8) ;
        }else{
            xInternal = 64 + (elem%8)
            x = String.fromCharCode(xInternal);
            y = parseInt(elem/8) + 1;
            
        }
        playerdisc = PLAYER_DISC.NO_DISC;
        cellStatus = CELL_STATUS.UNAVAILABLE;
        if ((x==="D" && y === 4) || (x==="E" && y === 5)){
            playerdisc=PLAYER_DISC.WHITE;
            cellStatus = CELL_STATUS.UNAVAILABLE;
        }
        if((x==="D" && y === 5)|| (x==="E" && y === 4)){
            playerdisc=PLAYER_DISC.BLACK;
            cellStatus = CELL_STATUS.UNAVAILABLE;
        }
        return {idx:idx,xInternal:xInternal,x:x,y:y,value:"",playerDisc:playerdisc,cellStatus:cellStatus}
    })
}

export const lookup = cells=> currentCell => direction =>{
    let xToFind,yToFind;
    xToFind = currentCell.xInternal + direction.x;
    yToFind = currentCell.y + direction.y;
    let foundCell = cells.filter(elem=>elem.xInternal === xToFind && elem.y === yToFind);
    return foundCell.length > 0 ? foundCell[0] : {idx:-1,xInternal:-1,x:-1,y:-1,value:"",playerDisc:"",cellStatus:CELL_STATUS.OUT_OF_GRID}
}


export function getEdgeCellsIndex(cells,playerDisc){
    let lookTable = lookup(cells)
    return cells.filter(elem=>elem.playerDisc ===playerDisc).reduce((acc,cur,idx,tbl)=>{
        let lookAround = lookTable(cur);
            if (lookAround(DIRECTION.LEFT).playerDisc === PLAYER_DISC.NO_DISC ||
            lookAround(DIRECTION.RIGHT).playerDisc === PLAYER_DISC.NO_DISC ||
            lookAround(DIRECTION.DOWN).playerDisc === PLAYER_DISC.NO_DISC ||
            lookAround(DIRECTION.UP).playerDisc === PLAYER_DISC.NO_DISC ||
            lookAround(DIRECTION.DIAG_BOTTOM_LEFT).playerDisc === PLAYER_DISC.NO_DISC ||
            lookAround(DIRECTION.DIAG_BOTTOM_RIGHT).playerDisc === PLAYER_DISC.NO_DISC ||
            lookAround(DIRECTION.DIAG_TOP_LEFT).playerDisc === PLAYER_DISC.NO_DISC ||
            lookAround(DIRECTION.DIAG_TOP_RIGHT).playerDisc === PLAYER_DISC.NO_DISC
            ){
                acc.push(cur.idx);
            }
        return acc;
    
    },[]);
}

export function findAvailableCellsToClick(cells,playerDisc){
    let edgeCellIndexes = getEdgeCellsIndex(cells,playerDisc); 
    let outsideIndex=[];
    for(let i=0;i<edgeCellIndexes.length;i++){
        let lookAround = lookup(cells)(cells[edgeCellIndexes[i]]);
        if (lookAround(DIRECTION.LEFT).playerDisc === PLAYER_DISC.NO_DISC){
            outsideIndex.push(lookAround(DIRECTION.LEFT).idx);
        }
        if (lookAround(DIRECTION.RIGHT).playerDisc === PLAYER_DISC.NO_DISC){
            outsideIndex.push(lookAround(DIRECTION.RIGHT).idx);
        }
        if (lookAround(DIRECTION.UP).playerDisc === PLAYER_DISC.NO_DISC){
            outsideIndex.push(lookAround(DIRECTION.UP).idx);
        }
        if (lookAround(DIRECTION.DOWN).playerDisc === PLAYER_DISC.NO_DISC){
            outsideIndex.push(lookAround(DIRECTION.DOWN).idx);
        }
        if (lookAround(DIRECTION.DIAG_BOTTOM_LEFT).playerDisc === PLAYER_DISC.NO_DISC){
            outsideIndex.push(lookAround(DIRECTION.DIAG_BOTTOM_LEFT).idx);
        }
        if (lookAround(DIRECTION.DIAG_BOTTOM_RIGHT).playerDisc === PLAYER_DISC.NO_DISC){
            outsideIndex.push(lookAround(DIRECTION.DIAG_BOTTOM_RIGHT).idx);
        }
        if (lookAround(DIRECTION.DIAG_TOP_LEFT).playerDisc === PLAYER_DISC.NO_DISC){
            outsideIndex.push(lookAround(DIRECTION.DIAG_TOP_LEFT).idx);
        }
        if (lookAround(DIRECTION.DIAG_TOP_RIGHT).playerDisc === PLAYER_DISC.NO_DISC){
            outsideIndex.push(lookAround(DIRECTION.DIAG_TOP_RIGHT).idx);
        }
    }
    return outsideIndex;
}

export const checkIfCellIsPlayable =(grid,playerDisc) => (originalCell,direction,nextCell=undefined,iteration = 0)=>{
    let oppositePlayerDisc = playerDisc === PLAYER_DISC.BLACK ? PLAYER_DISC.WHITE : PLAYER_DISC.BLACK;
    let cell = nextCell=== undefined ? originalCell : nextCell;
    let lookUpTable = lookup([...grid])(cell);
    let lookupCell = lookUpTable(direction);
    if(lookupCell.cellStatus===CELL_STATUS.OUT_OF_GRID){
        return false;
    }
    switch(lookupCell.playerDisc){
        case oppositePlayerDisc:
            return checkIfCellIsPlayable(grid,playerDisc)(originalCell,direction,lookupCell,iteration + 1);
        case playerDisc:
            if(iteration===0) return false
            else return true;
        default:
            return false;
    }
}

export function checkForAvailableCell(g,playerDisc){
    let oppositePlayerDisc = playerDisc === PLAYER_DISC.BLACK ? PLAYER_DISC.WHITE : PLAYER_DISC.BLACK;
    let outsideIndexes = findAvailableCellsToClick([...g], oppositePlayerDisc);

    let checkUpCell = checkIfCellIsPlayable(g,playerDisc);
    for(let i=0;i<outsideIndexes.length;i++){
        let originalCell = g[outsideIndexes[i]];
        if (checkUpCell(originalCell,DIRECTION.LEFT)){
            g[outsideIndexes[i]].cellStatus = CELL_STATUS.AVAILABLE;

        }else if(checkUpCell(originalCell,DIRECTION.RIGHT)){
            g[outsideIndexes[i]].cellStatus = CELL_STATUS.AVAILABLE;

        }else if(checkUpCell(originalCell,DIRECTION.UP)){
            g[outsideIndexes[i]].cellStatus = CELL_STATUS.AVAILABLE;

        }else if(checkUpCell(originalCell,DIRECTION.DOWN)){
            g[outsideIndexes[i]].cellStatus = CELL_STATUS.AVAILABLE;

        }else if(checkUpCell(originalCell,DIRECTION.DIAG_BOTTOM_LEFT)){
            g[outsideIndexes[i]].cellStatus = CELL_STATUS.AVAILABLE;

        }else if(checkUpCell(originalCell,DIRECTION.DIAG_BOTTOM_RIGHT)){
            g[outsideIndexes[i]].cellStatus = CELL_STATUS.AVAILABLE;

        }else if(checkUpCell(originalCell,DIRECTION.DIAG_TOP_LEFT)){
            g[outsideIndexes[i]].cellStatus = CELL_STATUS.AVAILABLE;

        }else if(checkUpCell(originalCell,DIRECTION.DIAG_TOP_RIGHT)){
            g[outsideIndexes[i]].cellStatus = CELL_STATUS.AVAILABLE;

        }else{
            g[outsideIndexes[i]].cellStatus = CELL_STATUS.UNAVAILABLE;
        }
    }
    return g;
}

export const checkForFlip =(newCell)=>(grid,direction,nextCell=undefined,trail=[])=>{
    let oppositePlayerDisc = newCell.playerDisc === PLAYER_DISC.BLACK ? PLAYER_DISC.WHITE : PLAYER_DISC.BLACK;
    let cell = nextCell=== undefined ? newCell : nextCell;
    let lookUpTable = lookup([...grid])(cell);
    let lookupCell = lookUpTable(direction);
   
    if (lookupCell.playerDisc === oppositePlayerDisc){
        trail.push(lookupCell.idx);
    }
    else if(lookupCell.playerDisc === newCell.playerDisc){
        for(let i=0;i<trail.length;i++){
            grid[trail[i]].playerDisc = newCell.playerDisc;
        }
        return grid;
    }else if(lookupCell.cellStatus === CELL_STATUS.OUT_OF_GRID){
        return grid;
    }else if(lookupCell.playerDisc === PLAYER_DISC.NO_DISC){
        return grid;
    }
    
    return checkForFlip(newCell)(grid,direction,lookupCell,trail);
    
    // if(lookupCell.cellStatus===CELL_STATUS.OUT_OF_GRID){
    //     return false;
    // }
    // switch(lookupCell.playerDisc){
    //     case oppositePlayerDisc:
    //         return checkForFlip(grid,playerDisc)(originalCell,direction,lookupCell,iteration + 1);
    //     case playerDisc:
    //         if(iteration===0) return false
    //         else return true;
    //     default:
    //         return {};
    // }
}

// export function flipDisc(direction,grid,newCell,nextCell=undefined){
//     let oppositePlayerDisc = newCell.playerDisc === PLAYER_DISC.BLACK ? PLAYER_DISC.WHITE : PLAYER_DISC.BLACK;
//     let cell = nextCell === undefined ? newCell : nextCell;
    
//     let lookupCell = lookup(grid)(cell);
//     let leftCell = lookupCell(direction);
    
// }