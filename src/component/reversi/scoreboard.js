import React, { Component } from 'react'
import {PLAYER_DISC} from '../../utils/static';
import './scoreboard.css';
class Scoreboard extends Component{
    render(){
        return(
            <div className="scoreboard">
                <h2>Player one:</h2>
                {this.props.grid.filter(elem=>elem.playerDisc === PLAYER_DISC.WHITE).length}
                <h2>Player two:</h2>
                {this.props.grid.filter(elem=>elem.playerDisc === PLAYER_DISC.BLACK).length}
            </div>
        );
    }
}

export default Scoreboard;