import React, { Component } from 'react'
import {PLAYER_DISC} from '../../utils/static';
class Cell extends Component{
    render(){
        return(
            <div onClick={(ev)=>this.props.cellClick(ev,this.props.tblindex)}>
                {this.props.playerdisc===PLAYER_DISC.NO_DISC && 
                    <span className={[this.props.playerdisc,this.props.cellStatus].join(' ')}>{this.props.XPOS} - {this.props.YPOS}</span>
                }
                {this.props.playerdisc !==PLAYER_DISC.NO_DISC &&
                    <span className={[this.props.playerdisc,this.props.cellStatus].join(' ')}></span>
                }
                
            </div>
        );
    }
}

export default Cell;