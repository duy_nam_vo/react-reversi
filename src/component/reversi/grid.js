import React, { Component } from 'react';
import './grid.css';
import Cell from './cell'
import Scoreboard from './scoreboard'; 
import {PLAYER_DISC,CELL_STATUS,PLAYER,DIRECTION} from '../../utils/static';
import {initialCellState,checkForAvailableCell,checkForFlip} from '../../utils/index';
import {lastState} from '../../utils/state';
class Grid extends Component{
    constructor(props){
        super(props);
        this.state={player:PLAYER.PLAYER_ONE,grid: initialCellState()}
        this.cellClick = this.cellClick.bind(this);
        this.routeTesting = this.routeTesting.bind(this);
    }
    
    routeTesting(){
        let newGrid = lastState.grid;
        let playerTurn = lastState.player;
        this.setState(()=>{
            return {grid:newGrid,player:playerTurn}
        },()=>{
            let playerDisc = this.state.player ===PLAYER.PLAYER_ONE? PLAYER_DISC.WHITE : PLAYER_DISC.BLACK;
            let newGrid = checkForAvailableCell([...this.state.grid],playerDisc);
            this.setState(()=>{ 
                return {grid:newGrid}
            });
        });
    }

    cellClick(event,position){
        if(this.state.grid[position].cellStatus === CELL_STATUS.UNAVAILABLE){
            return;
        }
        console.log(`x:${this.state.grid[position].x},y:${this.state.grid[position].y}`)
        let g = Object.assign([],[...this.state.grid]);
        let playerturn = this.state.player === PLAYER.PLAYER_ONE ? PLAYER.PLAYER_TWO : PLAYER.PLAYER_ONE;
            
        g[position].playerDisc = this.state.player === PLAYER.PLAYER_ONE ? PLAYER_DISC.WHITE: PLAYER_DISC.BLACK;
        g[position].cellStatus = CELL_STATUS.UNAVAILABLE;
        let fnFlip =  checkForFlip(g[position])
        let newGrid = fnFlip([...g],DIRECTION.LEFT);
        newGrid = fnFlip([...newGrid],DIRECTION.RIGHT);
        newGrid = fnFlip([...newGrid],DIRECTION.UP);
        newGrid = fnFlip([...newGrid],DIRECTION.DOWN);
        newGrid = fnFlip([...newGrid],DIRECTION.DIAG_BOTTOM_LEFT);
        newGrid = fnFlip([...newGrid],DIRECTION.DIAG_BOTTOM_RIGHT);
        newGrid = fnFlip([...newGrid],DIRECTION.DIAG_TOP_LEFT);
        newGrid = fnFlip([...newGrid],DIRECTION.DIAG_TOP_RIGHT);
        newGrid = newGrid.map(elem=>Object.assign({},elem,{cellStatus:CELL_STATUS.UNAVAILABLE}));
        // console.log(JSON.stringify({grid:newGrid,player:playerturn}));
        this.setState(()=>{
            return {grid:newGrid,player:playerturn}
        },()=>{
            let playerDisc = this.state.player ===PLAYER.PLAYER_ONE? PLAYER_DISC.WHITE : PLAYER_DISC.BLACK;
            let newGrid = checkForAvailableCell([...this.state.grid],playerDisc);
            this.setState(()=>{ 
                return {grid:newGrid}
            });
        });
    }

    componentDidMount(){
        let playerDisc = this.state.player === PLAYER.PLAYER_ONE ? PLAYER_DISC.WHITE : PLAYER_DISC.BLACK;
        let g = checkForAvailableCell([...this.state.grid],playerDisc);

        this.setState(()=>{
            return {grid:g}
        })
        //this.routeTesting();
    }
  
    render(){
        return(
            <div className="reversi-content">

                <div><h1>{this.state.player}</h1></div>
                <Scoreboard grid={this.state.grid} />
                <div className='grid'>
               
               {this.state.grid.map((elem,idx)=><Cell 
                    playerdisc = {elem.playerDisc}
                    cellStatus = {elem.cellStatus}
                    tblindex ={idx}
                    XPOS = {elem.x}
                    YPOS = {elem.y} 
                    value={elem.value} 
                    key={idx}
                    cellClick = {this.cellClick}
                    />)}
                </div>
            </div>
            
        );
    }
}

export default Grid;